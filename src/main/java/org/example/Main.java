package org.example;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine().toLowerCase();
        List<String> words = new ArrayList<>();

        // parse
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '.'
                    || string.charAt(i) == ','
                    || string.charAt(i) == '!'
                    || string.charAt(i) == ' '
                    || string.charAt(i) == '-'
                    || string.charAt(i) == ';'
                    || string.charAt(i) == '"') {
                if (sb.length() != 0) {
                    words.add(sb.toString());
                    sb.setLength(0);
                }
            } else {
                sb.append(string.charAt(i));
            }
        }
        if (sb.length() != 0)
            words.add(sb.toString());


        HashMap<String, Integer> counter = new HashMap<>();
        words.stream()
                .peek(word -> counter.put(word, counter.getOrDefault(word, 0) + 1))
                .collect(Collectors.toMap(
                        word -> word,
                        counter::get,
                        (oldValue, newValue) -> newValue
                ))
                .entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue(Comparator.reverseOrder())
                        .thenComparing(Map.Entry::getKey))
                .limit(10)
                .forEach(entry -> System.out.println(entry.getKey()));
    }
}